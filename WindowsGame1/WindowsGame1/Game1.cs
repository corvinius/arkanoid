using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private SpriteFont font;
       
        private Button button;

        private GameObject ball;
        private GameObject _player;
        private Rectangle _viewPortRectangle;
        private Texture2D _backgroudTexture2D;
        private int _brickPanelWidth = 10;
        private int _brickPanelHeight = 5;
        private Texture2D _brickTexture2D;
        private GameObject[,] _bricksGameObjects;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //graphics.PreferredBackBufferWidth = 1920;
            //graphics.PreferredBackBufferHeight = 1080;
            //graphics.IsFullScreen = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            
            base.Initialize();
   
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            _player = new GameObject((Content.Load<Texture2D>(@"paddle")));
            _viewPortRectangle = new Rectangle(0,0,graphics.GraphicsDevice.Viewport.Width,graphics.GraphicsDevice.Viewport.Height);
            _backgroudTexture2D = Content.Load<Texture2D>(@"back");
            _player.Position=new Vector2((_viewPortRectangle.Width-_player.width)/2.0f,(_viewPortRectangle.Height-_player.heigth-20));
            _player.StartPosition = _player.Position;
            _brickTexture2D = Content.Load<Texture2D>(@"brick");
            ball = new GameObject(Content.Load<Texture2D>(@"ball"));
            ball.Position = new Vector2((_viewPortRectangle.Width - ball.width)/2.0f,
                _viewPortRectangle.Height - _player.heigth - ball.heigth - 20);
            ball.StartPosition = ball.Position;
            ball.Velocity = new Vector2(-3, 3);
            _bricksGameObjects= new GameObject[_brickPanelWidth,_brickPanelHeight];
            for (var i = 0; i < _brickPanelWidth; i++)
            {
                for (var j = 0; j < _brickPanelHeight; j++)
                {
                    _bricksGameObjects[i, j] = new GameObject(_brickTexture2D)
                    {
                        Position = new Vector2(i*55 + 120, j*25 + 100)

                    };
               //     _bricksGameObjects[i, j].StartPosition = _bricksGameObjects[i, j].Position;
                }
            }
            IsMouseVisible = true;
            button=new Button(Content.Load<Texture2D>(@"paddle"),graphics.GraphicsDevice);
            button.SetPosition(new Vector2(0,0));
            font = Content.Load<SpriteFont>(@"font");
            //graphics.PreferredBackBufferWidth=scree

        }

      
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            KeyboardState state = Keyboard.GetState();
            if (state.IsKeyDown(Keys.D))
                _player.Position.X += 6f;
            if (state.IsKeyDown(Keys.A))
            {
                _player.Position.X -= 6f;
              //  Debug.WriteLine(2);

            }
            button.Update(mouse,Restart);
           // if(_player.Position.X==0)
           //     Debug.WriteLine(2);
            _player.Position.X = MathHelper.Clamp(_player.Position.X, 0, _viewPortRectangle.Width - _player.width);
            // TODO: Add your update logic here
            UpdateBall();
            base.Update(gameTime);
        }

        private void UpdateBall()
        {
           //ball.Position += ball.Velocity;
            Rectangle nextRectangle= new Rectangle((int)(ball.Position.X+ball.Velocity.X),(int)(ball.Position.Y+ball.Velocity.Y),ball.width,ball.heigth);
            if(nextRectangle.Y<=0)
                ball.ReflectionHorizontal();


            if ((nextRectangle.X >= _viewPortRectangle.Width - nextRectangle.Width) || nextRectangle.X <= 0)
            {
                ball.ReflectionVertical();
            }
            if (nextRectangle.Y >= _viewPortRectangle.Height - nextRectangle.Height)
            {
                ball.isAlive = false;
            }
            if(nextRectangle.Intersects(_player.Bounds))
                Collide(ball,_player.Bounds);
            foreach (var bricks in _bricksGameObjects)
            {
                if (nextRectangle.Intersects(bricks.Bounds) && bricks.isAlive)
                {
                    bricks.isAlive = false;
                    Collide(ball,bricks.Bounds);
                }
            }
            ball.Position += ball.Velocity;

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>S
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            spriteBatch.Draw(_backgroudTexture2D,_viewPortRectangle,Color.White);
            spriteBatch.Draw(_player.Sprite,_player.Position,Color.White);
            foreach (var variable in _bricksGameObjects)
            {
                if(variable.isAlive)
                    spriteBatch.Draw(variable.Sprite,variable.Position,Color.White);
            }
            spriteBatch.Draw(ball.Sprite,ball.Position,Color.White);
            if(!ball.isAlive)
            button.Draw(spriteBatch,"restart",font);
         // spriteBatch.DrawString(font,"sdgf",new Vector2(100,100),Color.White );
            // TODO: Add your drawing code here
            spriteBatch.End();
            
            base.Draw(gameTime);
        }

        public void Collide(GameObject gameObject,Rectangle rectangle)
        {
            if(rectangle.Left<=gameObject.Bounds.Center.X&&gameObject.Bounds.Center.X<=rectangle.Right)
                gameObject.ReflectionHorizontal();
            else if(rectangle.Top<=gameObject.Bounds.Center.Y&&gameObject.Bounds.Center.Y<=rectangle.Bottom)
                gameObject.ReflectionVertical();
        }

        public void Restart()
        {
            foreach (var bricksGameObject in _bricksGameObjects)
            {
                bricksGameObject.isAlive = true;
            }
            ball.Position = ball.StartPosition;
            ball.isAlive = true;
            _player.Position = _player.StartPosition;
        }
    }
}
