﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace WindowsGame1
{
    public delegate  void DoButton();
    class Button
    {
        private Texture2D texture;
        private Vector2 position;
        private Rectangle rectangle;

        private bool down;
        public bool IsClicked;

        public void Update(MouseState mouse,DoButton work)
        {
            rectangle = new Rectangle((int) position.X, (int) position.Y, (int) size.X, (int) size.Y);
            Rectangle Mouserectangle = new Rectangle(mouse.X, mouse.Y, 1, 1);
            if (Mouserectangle.Intersects(rectangle))
            {
                if (mouse.LeftButton == ButtonState.Pressed&&IsClicked)
                {
                    IsClicked = false;
                    work();
                }
                else if (mouse.LeftButton == ButtonState.Released)
                {
                    IsClicked = true;
                }
            }
        }

        public void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
        }

        public void Draw(SpriteBatch batch,string text,SpriteFont font)
        {
            batch.Draw(texture,rectangle,color);
            batch.DrawString(font,text,position, Color.White);
        }

        Color color=new Color(255,255,255,255);

        public Vector2 size;

        public Button(Texture2D newTexture2D, GraphicsDevice graphics)
        {
            texture=newTexture2D;
            size=new Vector2(graphics.Viewport.Width/8,graphics.Viewport.Height/30);

        }

    }
}
