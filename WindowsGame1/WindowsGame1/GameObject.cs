﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace WindowsGame1
{
    public class GameObject
    {
  
        public Texture2D Sprite { get; set; }
        public Vector2 StartPosition;
        public Vector2 Position;
        public Vector2 Velocity;
        public int width { get { return Sprite.Width; } }
        public int heigth { get { return Sprite.Height; } }
        public bool isAlive { get; set; }

        public Rectangle Bounds => new Rectangle((int)Position.X,(int)Position.Y,width,heigth);

        public void ReflectionHorizontal()
        {
            Velocity.Y = -Velocity.Y;
        }

        public void ReflectionVertical()
        {
            Velocity.X = -Velocity.X;
        }

        public GameObject(Texture2D sprite)
        {
            Sprite = sprite;
            isAlive = true;
            Position = Vector2.Zero;
            Velocity = Vector2.Zero;
        }
    }
}
